(ns nyarlathotep.core)

(defn rand-op []
  (rand-nth ['+ '- '* '/]))

(defn rand-num []
  (->> (range 1 101)
       (map #(/ % 10))
       rand-nth))

(defn rand-var [vars]
  (rand-nth vars))

(defn rand-subfn [vars]
  (let [op  (rand-op)
        var (if (> (Math/random) 0.5)
              (rand-num)
              (rand-var vars))]
    [op var]))

(defn expand-exp [exp vars]
  (let [[op var] (rand-subfn vars)]
    (list op var exp)))

(defn rand-exp [vars layers]
  (loop [exp 1, layer layers]
    (if (zero? layer)
      exp
      (recur (expand-exp exp vars) (dec layer)))))

(defn rand-fn [vars exp-layers]
  (let [exp (rand-exp vars exp-layers)]
    `(fn ~vars
       ~exp)))

(defn rand-fn-of [vars exp-layers]
  (eval (rand-fn vars exp-layers)))

(defn first-fitting [test-fn rand-fn-gen]
  (first (for [f (repeatedly rand-fn-gen)
               :when (try (test-fn f)
                       (catch Exception e false))]
           f)))
