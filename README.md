# Nyarlathotep

![Nyarlathotep](https://lovecraftianscience.files.wordpress.com/2014/11/shot_nyarlathotep_movie_director_christian_matzke_2001.jpg)

> “And where Nyarlathotep went, rest vanished, for the small hours were rent with the screams of nightmare.”
> - from [Nyarlathotep](http://www.hplovecraft.com/writings/texts/fiction/n.aspx) by H.P. Lovecraft

Constraint satisfying mathematical function generator.

## Usage

Suppose you want a function that satisfies the following constraints:
```
f(7)  = 49
f(11) = 121
```

This is how you would go about it:
```clojure
;; Constraints
(defn sqr-fn? [f]
  (and (= (f 7)   49)
       (= (f 11) 121)))

;; Returns a random f(x) that is 2 layers deep, eg:
;;    f(x) = (/ 2 (- x 1))
;;      or = (* x (+ x 0))
(defn rand-sqr-fn []
  (rand-fn-of '[x] 2))

;; This particular example takes 1 - 125 msecs.
(def sqr (first-fitting sqr-fn?
                        rand-sqr-fn))

(sqr 5) ;=> 25
```

## License

Copyright © Divyansh Prakash 2015

Distributed under the Eclipse Public License.
