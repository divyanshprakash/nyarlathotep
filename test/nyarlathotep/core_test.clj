(ns nyarlathotep.core-test
  (:require [clojure.test :refer :all]
            [nyarlathotep.core :refer :all]))

;; f( x ) = x * x
;; ==============
(defn sqr-fn? [f]
  (and (= (f 7)   49)
       (= (f 11) 121)))

(defn rand-sqr-fn []
  (rand-fn-of '[x] 2))

(deftest sqr-test
  (let [sqr (first-fitting sqr-fn?
                           rand-sqr-fn)]
    (is (= (sqr 3) 9))))


;; f( x ) = x * x * 2
;; ==================
(defn sqr-dbl-fn? [f]
  (and (= (f 1) 2)
       (= (f 2) 8)
       (= (f 3) 18)))

(defn rand-sqr-dbl-fn []
  (rand-fn-of '[x] 3))

(deftest sqr-dbl-test
  (let [sqr-dbl (first-fitting sqr-dbl-fn?
                               rand-sqr-dbl-fn)]
    (is (= (sqr-dbl 5) 50))))
