(defproject nyarlathotep "0.1.0-SNAPSHOT"
  :description "Constraint satisfying mathematical function generator."
  :url ""
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.6.0"]])
